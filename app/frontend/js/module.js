/**
 * @author bhushan
 */

//main js file which will initiate the angular module and other services
var rentat = angular.module('rentat', ['ngRoute', 'ngFileUpload', 'ngResource', 'raAppControllers', 'raServices', 'raAppDirectives', 'raAppFilters']);

rentat.config(['$routeProvider', '$locationProvider', '$httpProvider',
    function ($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider.when('/', {
            templateUrl: 'html/home.html',
            controller: 'homeCtrl',
            access: {restricted: false}
        }).when('/login', {                             //login page
                templateUrl: 'html/login.html',
                controller: 'loginController',
                access: {restricted: false}
            }).when('/login/:_id', {
                templateUrl: 'html/login.html',
                controller: 'loginController',
                access: {restricted: false}
            }).when('/signup', {                        //signup page
                templateUrl: 'html/signup.html',
                controller: 'signupController',
                access: {restricted: false}
            }).when('/post-ad', {                       //post ad page
                templateUrl: 'html/post-ad.html',
                controller: 'postAdController',
                access: {restricted: true}
            }).when('/about', {                         //about us
                templateUrl: 'html/about.html',
                access: {restricted: false}
            }).when('/contact', {                       //contact us
                templateUrl: 'html/contact.html',
                controller: 'contactController',
                access: {restricted: false}
            }).when('/faq', {                           //faq
                templateUrl: 'html/faq.html',
                access: {restricted: false}
            }).when('/terms', {                         //terms and conditions
                templateUrl: 'html/terms.html',
                access: {restricted: false}
            }).when('/privacy', {                       //privacy
                templateUrl: 'html/privacy.html',
                access: {restricted: false}
            }).when('/account', {                       //my account page
                templateUrl: 'html/account.html',
                controller: 'accountController',
                access: {restricted: true}
            }).when('/myads', {                         //my ads page for seller to manage their ads
                templateUrl: 'html/myads.html',
                controller: 'myadsController',
                reloadOnSearch: false,
                access: {restricted: true}
            }).when('/browse/:category', {              //ad listing page by category
                templateUrl: 'html/category.html',
                controller: 'category',
                access: {restricted: false}
            }).when('/ad/:adId', {                      //ad detail preview page
                templateUrl: 'html/productDetail.html',
                controller: 'adPreviewController',
                access: {restricted: true}
            }).when('/ad/:adId/preview', {                      //ad detail preview page
                templateUrl: 'html/productDetail.html',
                controller: 'adDetailController',
                access: {restricted: true}
            }).when('/previewAd/:adId/:approve', {                      //ad detail preview page
                templateUrl: 'html/productDetail.html',
                controller: 'adPreviewController',
                access: {restricted: true}
            }).when('/ad/:adId/edit', {                 //ad detail edit page for seller
                templateUrl: 'html/edit-ad.html',
                controller: 'adEditController',
                access: {restricted: true}
            }).when('/browse/seller/:sellerId', {       //ad listing page by seller
                templateUrl: 'html/seller-list.html',
                controller: 'adBySellerController',
                access: {restricted: false}
            }).when('/verify_mail/:code', {             //verification of email page
                templateUrl: 'html/verifyMail.html',
                controller: 'verificationController',
                access: {restricted: false}
            }).when('/seller/:sellerId', {             //verification of email page
                templateUrl: 'html/category.html',
                controller: 'sellerListController',
                access: {restricted: false}
            }).otherwise({
                redirectTo: '/'
            });

        // use the HTML5 History API
        // $locationProvider.html5Mode(true);

        $httpProvider.interceptors.push('myHttpInterceptor');
    }]);


rentat.run(function ($rootScope, $location, $route, authService, $timeout, $routeParams) {

    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        console.log('here in route change start==');
        $rootScope.globalError = {};
        $timeout(function () {
            if ($rootScope.loginError) {
                $rootScope.loginError.showLoginError = false;
            }
        }, 2000);
        if ((next.access && next.access.restricted)) {
            // show alert that you are not logged in
            authService.isLoggedIn();
//            console.log('=====a=====', a, $rootscope.userpresent);
            // if(!$rootscope.userpresent){
            // $location.path('/login');
            // }
        }
        else {
            authService.isLoggedIn();
        }
    });
    $rootScope.$on('$stateChangeError', function () {

    })
});

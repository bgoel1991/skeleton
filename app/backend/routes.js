/**
 * Created with JetBrains WebStorm.
 * User: Bhushan
 * Date: 9/4/15
 * Time: 11:23 PM
 * To change this template use File | Settings | File Templates.
 */


//main routes file
var express = require('express')
    , router = express.Router();

//for authentication of api at backend >> api should start with /api/*
router.use(require('./routes/authentication'));

router.use(require('./routes/categories'));
router.use(require('./routes/login'));
router.use(require('./routes/contact'));
router.use(require('./routes/signup'));
router.use(require('./routes/post-ad'));
router.use(require('./routes/user-details'));
router.use(require('./routes/verification'));
router.use(require('./routes/accounts'));
router.use(require('./routes/ads'));

module.exports = router;

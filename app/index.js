var express = require("express");
var app = module.exports = express();
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var db = require("ra-db");
var rentat = require("ra-services");
var passport = require('passport');
// var LocalStrategy = require('passport-local').Strategy;

// Connection URL >> connecting with mongodb
var url = 'mongodb://localhost:27017/rentat';
//var url = 'mongodb://52.76.0.111:27017/rentat';
// Use connect method to connect to the Server
db.connect(url, function (err) {
    console.log('connected...')
});

// serve static files
app.use(express.static(__dirname + '/frontend'));

app.use(bodyParser.urlencoded({'extended': 'false'})); 			// parse application/x-www-form-urlencoded
app.use(bodyParser.json());

//passport initialisation
app.use(session({ secret: 'abilliondollar', resave: true,  saveUninitialized: true})); // session secret
app.use(passport.initialize());
app.use(passport.session());

//app.use('/', require('./backend/routes/categories'));
app.use(require('./backend/ra-routes'));

app.use('/', function (req, res, next) {
    res.sendFile(__dirname + '/frontend/index.html');
});

var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('app listening at http://%s:%s', host, port);
});